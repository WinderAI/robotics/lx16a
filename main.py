import time

from loguru import logger
import serial

from lx16a.protocol import *
from lx16a.servo import Servo

ser = serial.Serial("/dev/cu.usbserial-1420", 115200, timeout=1)
p = Protocol(serial=ser)
servo = Servo(protocol=p)

logger.info(f"We are connected to servo {servo.id}")
logger.info(f"The position of {servo.id} is {servo.get_position()}")
servo.set_position_blocking(150)
logger.info(f"The position of {servo.id} is {servo.get_position()}")
servo.prepare_position(850)
servo.start()
time.sleep(0.1)
logger.info(f"The position of {servo.id} is {servo.get_position()}")
time.sleep(0.1)
servo.stop()
logger.info(f"The position of {servo.id} is {servo.get_position()}")

servo.set_servo_id(3)
time.sleep(0.2)
logger.info(f"The position of {servo.id} is {servo.get_position()}")
p = Protocol(serial=ser)
logger.info(f"We are connected to servo {servo.id}")
servo.set_servo_id(2)

logger.info(f"Connected back to servo {servo.id}")
