from unittest import mock
import pytest

from lx16a.servo import Servo, Protocol, ServoCollection
from lx16a import constants


@pytest.fixture
def p():
    return mock.Mock(spec_set=Protocol)


def test_initialise_no_id(p):
    p.query.return_value = dict(id=1)
    s = Servo(protocol=p)
    assert s.id == 1


def test_get_set_id(p):
    s = Servo(protocol=p, servo_id=1)
    assert s.id == 1
    s.set_servo_id(2)
    p.command.assert_called_once_with(1, constants.SERVO_ID_WRITE, 2)
    assert s.id == 2


def test_get_set_position(p):
    p.query.return_value = dict(data=b"\x05\x00")
    s = Servo(protocol=p, servo_id=1)
    assert s.get_position() == 5
    s.set_position(100)
    p.command.assert_called_once_with(1, 1, 100)


def test_prepare_start_stop(p):
    s = Servo(protocol=p, servo_id=1)
    s.prepare_position(5)
    s.start()
    s.stop()
    assert p.command.call_count == 3


def test_servo_collection_start_stop_all(p):
    s = ServoCollection(protocol=p)
    s.start_all()
    p.command.assert_called_with(constants.SERVO_ID_ALL, constants.SERVO_MOVE_START)
    s.stop_all()
    p.command.assert_called_with(constants.SERVO_ID_ALL, constants.SERVO_MOVE_STOP)
    assert p.command.call_count == 2


def test_blocking_move(p):
    p.query.side_effect = [dict(data=b"\xFF\x00"), dict(data=b"\x05\x00")]
    s = Servo(protocol=p, servo_id=1)
    s.set_position_blocking(5)
    p.command.assert_called_once_with(1, 1, 5)
    assert p.query.call_count == 2
    p.query.assert_called_with(1, constants.SERVO_POS_READ)
